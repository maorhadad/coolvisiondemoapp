package com.hadadas.coolvisiondemoapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hadadas.coolvisiondemoapp.adapters.ListRowAdapter;
import com.hadadas.coolvisiondemoapp.pogos.PhotoList;
import com.hadadas.coolvisiondemoapp.utiles.Constants;
import com.hadadas.coolvisiondemoapp.volley.MyVolley;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();
    private ListView listView;
    private ListRowAdapter listRowAdapter;
    private PhotoList photoList;


    @Override
    protected void onResume(){
        super.onResume();
        fetchData();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    private void init(){

        listView = (ListView) findViewById(R.id.list);
        photoList = new PhotoList();
        listRowAdapter = new ListRowAdapter(this,R.layout.list_row_layout, photoList.getPhoto());
        listView.setAdapter(listRowAdapter);
    }
    private void fetchData(){
        Cache cache = MyVolley.getRequestQueue().getCache();
        Entry entry = cache.get(Constants.BASE_URL);
        if (entry != null) {
            // fetch the data from cache
            String data = null;
            try {
                data = new String(entry.data, "UTF-8");
                parseJsonFeed(data);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            makeApiCall();
        }
    }
    private void makeApiCall() {

        RequestQueue queue = MyVolley.getRequestQueue();
        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.BASE_URL,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {
        };
        int socketTimeout = 40000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
    }
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                parseJsonFeed(response);

            }
        };
    }
    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError");


            }
        };
    }

    private void  parseJsonFeed(String response){

        Gson gson = new Gson();
        photoList = gson.fromJson(response, PhotoList.class);
        listRowAdapter.addAll(photoList.getPhoto());


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
